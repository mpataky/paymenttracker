import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class CurrencyApiHandler {
    private static final String ACCESS_KEY = "e0e2eaa9e1d4d935fe61df188b3866c1";
    private static final String BASE_URL = "http://apilayer.net/api/";
    private static final String ENDPOINT = "live";

    private static final CloseableHttpClient httpClient = HttpClients.createDefault();

    public static Double sendLiveRequest(String currency) {
        HttpGet get = new HttpGet(BASE_URL + ENDPOINT + "?access_key=" + ACCESS_KEY + "&currencies=" + currency);

        try {
            CloseableHttpResponse response = httpClient.execute(get);
            HttpEntity entity = response.getEntity();

            JSONObject jsonResponse = new JSONObject(EntityUtils.toString(entity));
            if (jsonResponse.getBoolean("success")) {
                JSONObject exchangeRates = jsonResponse.getJSONObject("quotes");
                return exchangeRates.getDouble("USD" + currency);
            }
            response.close();
        } catch (IOException | ParseException | JSONException e) {
            System.out.println("Exchange rate for " + currency + " could not be loaded from API.");
        }
        return null;
    }


}
