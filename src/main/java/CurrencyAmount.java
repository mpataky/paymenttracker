import java.text.DecimalFormat;
import java.util.Locale;

public class CurrencyAmount {

    private long amount;
    private double exchangeRate;
    private String exchangeOutput;

    public CurrencyAmount(long amount, double exchangeRate) {
        this.amount = amount;
        this.exchangeRate = exchangeRate;
        this.buildExchangeOutput();
    }

    public CurrencyAmount(long amount) {
        this(amount, 0);
    }

    public double getExchangeRate() {
        return this.exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public long getAmount() {
        return this.amount;
    }

    public void addAmount(long amount) {
        this.amount = this.amount + amount;
        buildExchangeOutput();
    }

    public String getExchangeOutput() {
        return exchangeOutput;
    }

    private void buildExchangeOutput() {
        if (this.exchangeRate != 0) {
            this.exchangeOutput = " ( " + DecimalFormat.getCurrencyInstance(Locale.CANADA).format(this.amount / this.exchangeRate) + " )";
            //the US locale doesn't work for negative numbers
        } else {
            this.exchangeOutput = "";
        }
    }
}
