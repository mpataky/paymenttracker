import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PaymentTracker {

    private final Map<String, CurrencyAmount> currencyMap = new ConcurrentHashMap<>();
    private final Timer TIMER = new Timer();
    private final String CURRENCY_LINE_PATTERN = "^[A-Z]{3} -?\\d+$";
    private final String QUIT_COMMAND = "quit";

    public PaymentTracker() {
        this.currencyMap.put("USD", new CurrencyAmount(0));
    }

    private void processCurrencyRecordLine(String line) {
        if (line.matches(CURRENCY_LINE_PATTERN)) {
            String currency = line.substring(0, 3);
            long amount = Long.valueOf(line.substring(4));
            synchronized (this) {
                CurrencyAmount actualCurrencyAmount = currencyMap.get(currency);
                if (actualCurrencyAmount == null) {
                    Double currencyRate = CurrencyApiHandler.sendLiveRequest(currency);
                    CurrencyAmount newCurrencyAmount = new CurrencyAmount(amount, currencyRate == null ? 0 : currencyRate);
                    currencyMap.put(currency, newCurrencyAmount);
                } else {
                    actualCurrencyAmount.addAmount(amount);
                }
            }
        } else {
            System.out.println("The currency record format was wrong.");
        }
    }

    private void startInputProcessing() {
        Scanner scanner = new Scanner(System.in);
        String line;
        while (!(line = scanner.nextLine()).equals(QUIT_COMMAND)) {
            processCurrencyRecordLine(line);
        }
        TIMER.cancel();
    }

    private void setupTimer() {
        TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                final StringBuilder sb = new StringBuilder("-------------------------------------\n");
                currencyMap.entrySet().stream().filter((record) -> record.getValue().getAmount() != 0L).forEach((record) -> {
                    final CurrencyAmount currencyAmount = record.getValue();
                    sb.append(record.getKey()).append(" ").append(currencyAmount.getAmount()).append(currencyAmount.getExchangeOutput()).append("\n");
                });
                sb.append("-------------------------------------\n");
                System.out.print(sb.toString());
            }
        }, 1000 * 10, 1000 * 60);
    }

    private void loadCurrenciesFromFile(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                processCurrencyRecordLine(line);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File " + fileName + "was not found.\nStopping the application.");
            System.exit(1);
        }
    }

    public static void main(String[] args) {
        final PaymentTracker tracker = new PaymentTracker();
        if (args.length > 0) {
            tracker.loadCurrenciesFromFile(args[0]);
        }
        tracker.setupTimer();
        Thread readInputsThread = new Thread() {
            @Override
            public void run() {
                tracker.startInputProcessing();
            }
        };
        readInputsThread.start();
    }

}

