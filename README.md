
# Payment Tracker

+ [How to run it](#runit)
+ [Specification](#specification)

# How to run it <a name="runit"/>
Solution si based on *Maven*.

Please pull all codes from repository and set on the top project directory e.g. C:\Documents\Payment Tracker

Afterwards, use these two commands to run a solution:

+ `mvn compile`
+ `mvn exec:java -Dexec.mainClass="PaymentTracker"`


# Specification <a name="specification"/>
Program is waiting for user inputs. The Input format should contain 3 uppercase letters representing currency and the amount separated by 1 space.
If there is new currency, the program try to get the exchange rate through [Currency API](https://apilayer.com/). There is a limitation for free accounts, so the get for exchange rate information is done only once.
There is possibility to specify the file input using command: `mvn exec:java -Dexec.mainClass="PaymentTracker" -Dexec.args="pathToFile"` where pathToFile is your file location.